﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ButtonEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{

	public bool isPressed = false;

	public void OnPointerDown (PointerEventData eventData) 
	{
		isPressed = true;
	}
	public void OnPointerUp (PointerEventData eventData) 
	{
		isPressed = false;
	}

}
