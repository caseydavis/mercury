﻿using UnityEngine;
using System.Collections;

public class UIFadeController : MonoBehaviour {
	public delegate void OnTimerEvent();
	
	private OnTimerEvent CallBackFunction;          

	public void Hide(){
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		CG.alpha = 0F;
		gameObject.SetActive (false);
	}

	public void Hide(OnTimerEvent newCallBack){
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		CG.alpha = 0F;
		gameObject.SetActive (false);
		CallBackFunction = newCallBack;
		CallBackFunction ();
	}

	public void Show(){
		gameObject.SetActive (true);
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		CG.alpha = 1F;
	}
	
	public void Show(OnTimerEvent newCallBack){
		gameObject.SetActive (true);
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		CG.alpha = 1F;
		CallBackFunction = newCallBack;
		CallBackFunction ();
	}
	
	public void FadeIn(float delay, float speed, OnTimerEvent newCallBack){
		StartCoroutine(Fader("in", delay, speed, newCallBack));
	}

	public void FadeIn(float delay, float speed){
		StartCoroutine(Fader("in", delay, speed));
	}
	
	public void FadeOut(float delay, float speed, OnTimerEvent newCallBack){
		StartCoroutine(Fader("out", delay, speed, newCallBack));
	}

	public void FadeOut(float delay, float speed){
		StartCoroutine(Fader("out", delay, speed));
	}
	
	IEnumerator Fader(string inout, float delay, float speed, OnTimerEvent newCallBack){
		if (delay > 0) {
			yield return new WaitForSeconds(delay);
		}
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		switch(inout){
		case "in":
			CG.alpha = 0;
			while(CG.alpha < 1){
				CG.alpha += Time.deltaTime / speed;
				yield return null;
			}
			CallBackFunction = newCallBack;
			CallBackFunction ();
			yield return null;
			break;
		case "out":
			CG.alpha = 1;
			while(CG.alpha > 0){
				CG.alpha -= Time.deltaTime / speed;
				yield return null;
			}
			gameObject.SetActive (false);
			CallBackFunction = newCallBack;
			CallBackFunction ();
			yield return null;
			break;
		}
	}
	
	IEnumerator Fader(string inout, float delay, float speed){
		if (delay > 0) {
			yield return new WaitForSeconds(delay);
		}
		CanvasGroup CG = GetComponent<CanvasGroup> ();
		switch(inout){
		case "in":
			CG.alpha = 0;
			while(CG.alpha < 1){
				CG.alpha += Time.deltaTime / speed;
				yield return null;
			}
			yield return null;
			break;
		case "out":
			CG.alpha = 1;
			while(CG.alpha > 0){
				CG.alpha -= Time.deltaTime / speed;
				yield return null;
			}
			gameObject.SetActive (false);
			yield return null;
			break;
		}
	}
	
}
