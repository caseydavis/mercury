﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeckController : MonoBehaviour {

	public Texture BackFace;
	public Vector3 CardScale = new Vector3(1.0F, 1.6F, 1.0F);
	public List<Texture> Trumps;
	public List<Texture> MinorArcana;
	public List<GameObject> Deck;

	public void UpdateDeckPosition(Vector3 newPosition){
		for (int i = 0; i < Deck.Count; i++) {
//			iTween.MoveTo(Deck[i], iTween.Hash("position", new Vector3(newPosition.x, (newPosition.y-0.4F)+((float)i/100), newPosition.z), "time", 0, "delay", 0));
			iTween.MoveTo(Deck[i], iTween.Hash("position", newPosition, "time", 0, "delay", 0));
		}
	}

	public void CreateDeck(string spread){
		Deck = new List<GameObject>();
		if (spread == "TrumpsOnly" || spread == "TrumpsAndMinors") {
			for (int i = 0; i < Trumps.Count; i++) {
				GameObject go = Instantiate (Resources.Load ("Prefabs/CardBase", typeof(GameObject))) as GameObject;
				Renderer[] renderers = go.transform.GetComponentsInChildren<Renderer> ();
				Transform[] transformers = go.transform.GetComponentsInChildren<Transform> ();
				renderers [0].material.mainTexture = Trumps [i];
				renderers [1].material.mainTexture = BackFace;
				transformers [1].transform.localScale = CardScale;
				transformers [2].transform.localScale = CardScale;
				go.transform.parent = transform;
				Deck.Add (go);
			}
		}
		if (spread == "MinorsOnly" || spread == "TrumpsAndMinors") {
			for (int i = 0; i < MinorArcana.Count; i++) {
				GameObject go = Instantiate (Resources.Load ("Prefabs/CardBase", typeof(GameObject))) as GameObject;
				Renderer[] renderers = go.transform.GetComponentsInChildren<Renderer> ();
				Transform[] transformers = go.transform.GetComponentsInChildren<Transform> ();
				renderers [0].material.mainTexture = MinorArcana [i];
				renderers [1].material.mainTexture = BackFace;
				transformers [1].transform.localScale = CardScale;
				transformers [2].transform.localScale = CardScale;
				go.transform.parent = transform;
				Deck.Add (go);
			}
		}

	}
}
