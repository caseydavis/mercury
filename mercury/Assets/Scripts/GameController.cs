﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public GameObject DealerPrefab; 
	public GameObject ButtonPrefab;
	public GameObject ScrollContent;
	public float ButtonHeight;

	public List<GameObject> DeckPrefabs; 
	public List<GameObject> SpreadPrefabs;

	protected GameObject Dealer = null;
	protected DealerController dc = null;
	protected bool ShuffleStart = false;
	protected bool ShuffleUp = false;
	protected bool sPressed = true;
	protected bool DealDown = false;
	protected MusicManagerScript mms;
	protected FlythroughCameraController fcc;
	protected ScreenTransController stc;

	private GameObject ShuffleButton;
	private ButtonEvents sbbe;
	private Outline sbo;
	private float startPosition = 0.5f;
	private float tablePosition = 0f;
	private float bookPosition = 1;
	private int DeckIndex = 0;
	private int SpreadIndex = 0;
	private List<GameObject> btns = new List<GameObject>();


	void Start (){
		mms = GameObject.Find("MusicManager").GetComponent<MusicManagerScript> ();
		fcc = GameObject.Find("MusicCamera").GetComponent<FlythroughCameraController> ();
		stc = GameObject.Find("Canvas").GetComponent<ScreenTransController> ();
		ShuffleButton = GameObject.Find("ShuffleButton");
		sbbe = ShuffleButton.GetComponent<ButtonEvents> ();
		sbo = ShuffleButton.GetComponent<Outline> ();

		for (int i = 0; i < SpreadPrefabs.Count; i ++) {
			string text = SpreadPrefabs[i].name;
			StringBuilder newText = new StringBuilder(text.Length * 2);
			newText.Append(text[0]);
			for (int j = 1; j < text.Length; j++)
			{
				if (char.IsUpper(text[j]) && text[j - 1] != ' ')
					newText.Append(' ');
				newText.Append(text[j]);
			}
			newText = newText.Replace("Spread", "");
			AddSpreadButton(newText.ToString());
		}
	}
	
	void Update () {
		if (ShuffleUp) {
			if (ShuffleStart){
				ShuffleStart = false;
				if (dc == null) {
					SpawnDealer (DeckIndex, SpreadIndex);
				} else {
					KillDealer ();
					SpawnDealer (DeckIndex, SpreadIndex);
				}
			}
			if(dc != null){
				if(dc.isDealerReady()){
					mms.PlayShuffle = true;
					dc.ShuffleDeck();
				}
			}
		}
		if (DealDown) {
			DealDown = false;
			ShuffleUp = false;
			mms.PlayShuffle = false;
			dc.DealCardsToSpread();
		}
		if (sbbe.isPressed && sPressed) {
			sPressed = false;
			sbo.enabled = true;
			ShuffleStart = true;
			ShuffleUp = true;
		}
	}

	public void SpawnDealer(int deckIndex, int spreadIndex){
		Dealer = Instantiate(DealerPrefab, DealerPrefab.transform.position, Quaternion.identity) as GameObject;
		dc = Dealer.GetComponent<DealerController>();
		dc.DeckPrefab = DeckPrefabs[deckIndex];
		dc.SpreadPrefab = SpreadPrefabs[spreadIndex];
	}

	public void KillDealer(){
		if (Dealer != null) {
			Destroy (Dealer);
			Dealer = null;
			dc = null;
		}
	}

	public void PlayEvent(){
		fcc.SlideTo (tablePosition);
		stc.FadeOutScreen (1, 0F, 0.5F);
		stc.FadeInScreen (2, 0F, 0.5F);
		stc.Screens[3].SetActive (true);
		stc.Screens[3].GetComponent<UIFadeController>().FadeIn(0F, 0.5F);
	}

	public void DiscoverEvent(){
		fcc.SlideTo (bookPosition);
	}
	
	public void BackEvent(){
		int preScreen = 0;
		if (stc.History.Count - 1 > 0) {
			preScreen = stc.History[stc.History.Count - 1];
		}
		switch (preScreen) {
			case 1:
			case 2:
				fcc.SlideTo(startPosition);
				if(stc.Screens[3].activeSelf){
					stc.Screens[3].GetComponent<UIFadeController>().FadeOut(0F, 0.5F);
				}
				stc.GoBack(new int[] {1});
			break;
			case 3:
				stc.GoBack(new int[] {2});
			break;
		}
	}

	public void ShuffleUpAndDeal(){
		ShuffleUp = false;
		DealDown = true;
		sPressed = true;
		sbo.enabled = false;
	}

	public void ChooseSpreadEvent(){
		if (!stc.Screens [3].activeSelf) {
			stc.FadeInScreen (3, 0F, 0.5F);
		} else {
			stc.FadeOutScreen (3, 0F, 0.5F);
		}
	}
	
	public void AddSpreadButton(string text){
		int ButtonCount = btns.Count;
		GameObject btn = Instantiate (ButtonPrefab) as GameObject;
		Button btnRef = btn.GetComponentInChildren<Button>();
		RectTransform scTransform = ScrollContent.GetComponent<RectTransform>();
		Text txt = btn.GetComponentInChildren<Text>();
		txt.text = text;
		btn.transform.SetParent(ScrollContent.transform);
		btnRef.onClick.AddListener (() => SpreadButtonListner (ButtonCount));
		btns.Add (btn);
		scTransform.sizeDelta = new Vector2 (scTransform.sizeDelta.x, (ButtonHeight * btns.Count) - 25);
	}
	
	public void SpreadButtonListner(int id){
		SpreadIndex = id;
		ChooseSpreadEvent ();
	}

}

