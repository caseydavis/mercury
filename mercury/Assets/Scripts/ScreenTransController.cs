﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScreenTransController : MonoBehaviour {

	public List<GameObject> Screens = new List<GameObject>();
	public List<int> History = new List<int>();

	void Start(){
		for (int i = 0; i < Screens.Count; i++) {
			Screens[i].SetActive(false);
			Screens[i].transform.position = new Vector3(0, 0, 0);
		}
		ShowScreen (0);
		ShowScreen (1);
		FadeOutScreen (0, 4F, 0.5F);
	}

	public void BreadCrumb(int index){
		History.Add(index);
		if (History.Count > 5) {
			History.RemoveAt(0);
		}
	}

	public void GoBack(int[] ScreenIndexs){
		for (int i = 0; i < Screens.Count; i++) {
			if(Screens[i].activeSelf){
				HideScreen(i);
			}
		}
		for (int i = 0; i < ScreenIndexs.Length; i++) {
			ShowScreen(ScreenIndexs[i]);
		}
	}

	public void FadeInScreens(int[] ScreenIndexs){
	}
	
	public void ShowScreen(int index){
		Screens[index].GetComponent<UIFadeController> ().Show();
		BreadCrumb (index);
	}
	
	public void HideScreen(int index){
		Screens[index].GetComponent<UIFadeController> ().Hide();
	}

	public void FadeInScreen(int index, float delay, float speed){
		Screens[index].SetActive (true);
		Screens[index].GetComponent<UIFadeController> ().FadeIn(delay, speed);
		BreadCrumb (index);
	}
	
	public void FadeOutScreen(int index, float delay, float speed){
		Screens[index].GetComponent<UIFadeController> ().FadeOut(delay, speed);
	}
}
