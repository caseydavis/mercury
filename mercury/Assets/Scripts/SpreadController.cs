﻿using UnityEngine;
using System.Collections;

public enum PullCards{TrumpsAndMinors, TrumpsOnly, MinorsOnly}
public enum Flip{Yes, No}

public class SpreadController : MonoBehaviour {

	public PullCards SetPullCards;
	public Flip AllowFlip; 
}
