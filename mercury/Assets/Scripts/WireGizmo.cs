﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class WireGizmo : MonoBehaviour {

	public Color GizmoColor = new Color(1, 1, 1, 1);

	void OnDrawGizmos () {
		Gizmos.matrix = transform.localToWorldMatrix; //Matrix4x4.TRS(transform.localPosition, transform.localRotation, transform.localScale);
		Gizmos.color = GizmoColor;
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
	}
}
