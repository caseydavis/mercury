﻿using UnityEngine;
using System.Collections;

public class MusicManagerScript : MonoBehaviour {

	public bool PlayShuffle = false;
	public AudioClip[] songs;

	private AudioSource[] sources;
	private AudioClip Shuffle;

	void Start(){
		Shuffle = Resources.Load("Music/tarot-shuffle", typeof(AudioClip)) as AudioClip;
		sources = GetComponents<AudioSource>();
	}

	void Update(){
		if (PlayShuffle) {
			if (!sources [0].isPlaying) {
				iTween.AudioTo(gameObject,1,1,0);
				sources [0].clip = Shuffle;
				sources [0].Play ();
			}
		} else {
			if (sources [0].isPlaying) {
				iTween.AudioTo(gameObject,0,1,0.8F);
			}
		}
		if(sources[1].isPlaying == false){
			sources[1].clip = songs [Random.Range(0,songs.Length-1)];
			sources[1].Play();
		}
	}
}
