using UnityEngine;
using System.Collections;

public class FlythroughCameraController : MonoBehaviour {
	public Transform[] movePath;
	public Transform[] lookPath;
	public Transform lookTarget;
	public float percentage;
	


	void Update(){
		iTween.PutOnPath (gameObject, movePath, percentage);
		iTween.PutOnPath (lookTarget, lookPath, percentage);
		transform.LookAt (iTween.PointOnPath (lookPath, percentage));
	}
	
	void OnDrawGizmos(){
		iTween.DrawPath(movePath,Color.magenta);
		iTween.DrawPath(lookPath,Color.cyan);
		Gizmos.color=Color.black;
		Gizmos.DrawLine(transform.position,lookTarget.position);
	}
	
	public void SlideTo(float position){
		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject,iTween.Hash("from",percentage,"to",position,"time",2,"easetype",iTween.EaseType.easeInOutCubic,"onupdate","SlidePercentage"));	
	}
	
	void SlidePercentage(float p){
		percentage=p;
	}
}
