﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DealerController : MonoBehaviour {

	public GameObject DeckPrefab;
	public GameObject SpreadPrefab;

	protected GameObject SelectedDeck;
	protected GameObject SelectedSpread;

	private DeckController dc;
	private SpreadController sc;
	private List<int> ShuffledIndex;
	private bool dealerReady = false;

	void Start () {
		if (DeckPrefab != null && SpreadPrefab != null) {
			SelectedDeck = Instantiate(DeckPrefab, DeckPrefab.transform.position, Quaternion.identity) as GameObject;
			SelectedSpread = Instantiate(SpreadPrefab, SpreadPrefab.transform.position, Quaternion.identity) as GameObject;
			SelectedDeck.transform.SetParent(transform, false);
			SelectedSpread.transform.SetParent(transform, true);
			dc = SelectedDeck.GetComponent<DeckController>();
			sc = SelectedSpread.GetComponent<SpreadController>();
			dc.CreateDeck(sc.SetPullCards.ToString());
			dc.UpdateDeckPosition(SelectedDeck.transform.position);
			dealerReady = true;
		}
	}
	public void ShuffleDeck(){
		ShuffledIndex = new List<int>();
		List<int> possibleNumbers = new List<int>(Enumerable.Range(0, dc.Deck.Count));
		System.Random rnd = new System.Random();
		while (possibleNumbers.Count > 0) {
			int r = rnd.Next(possibleNumbers.Count);
			ShuffledIndex.Add(possibleNumbers[r]);
			possibleNumbers.RemoveAt(r);
		}
	}
	public void DealCardsToSpread(){
		int ChildCount = SelectedSpread.GetComponentsInChildren<Transform>().Length - 1;
		float duration, delay, offset = 0.15F;
		duration = 0.5F;
		delay = 0.9F;
		for(int i = 0; i < ChildCount; i++){
			int flip = 0;
			if(sc.AllowFlip.ToString() == "Yes"){
				flip = Random.Range(0,Random.Range(1,4));
				if(flip > 0){
					flip = 180;
				}
			}
			iTween.MoveTo(dc.Deck[ShuffledIndex[i]], iTween.Hash("position", SelectedSpread.transform.GetChild(i).position, "time", duration, "delay", delay));
			iTween.RotateTo(dc.Deck[ShuffledIndex[i]], iTween.Hash("rotation", new Vector3(0,SelectedSpread.transform.GetChild(i).localEulerAngles.y+flip,-180), "time", duration-offset, "delay", delay+offset));
			delay = delay + duration;
		}
		for(int i = ChildCount; i < dc.Deck.Count; i++){
			dc.Deck[ShuffledIndex[i]].SetActive(false);
		}
	}
	public bool isDealerReady(){
		return dealerReady;
	}
}
